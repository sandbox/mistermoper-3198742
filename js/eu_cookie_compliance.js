/**
 * @file
 * Contains eu_cookie_compliance.js.
 */
(function ($, Drupal, window, document) {
  'use strict';

  Drupal.behaviors.google_tag_eu_cookie_compliance = {
    attach: function (context, settings) {
      once('google-tag-eu-cookie-compliance', 'body', context).forEach(function () {
        $(document).on('eu_cookie_compliance.changePreferences', function (e, categories) {
          Drupal.google_tag_eu_cookie_compliance.pushCategories(categories);
        });
      });
    }
  };

  Drupal.google_tag_eu_cookie_compliance = {};
  Drupal.google_tag_eu_cookie_compliance.pushCategories = function (categories) {
    window.dataLayer = window.dataLayer || [];

    for (var i = 0; i < categories.length; i++) {
      window.dataLayer.push({
        'event': 'eu_cookie_compliance.accept.' + categories[i]
      });
    }
  };

})(jQuery, Drupal, this, this.document);
